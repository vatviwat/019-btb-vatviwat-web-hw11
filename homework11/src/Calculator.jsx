import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card,Container,Row,Col,Form,Button, ListGroup} from 'react-bootstrap';
import History from './History'
export default class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      val1: '',
      val2: '',
      operator: '+ Plus',
      total: 0,
      numbers: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});
    console.log(this.state.val1);
  }
  handleValidation(){
    let fields = this.state;
    let isValid = true;
    if(!fields["val1"]){
      isValid = false;
      alert("Please input first value");
    }
    else if(!fields["val2"]){
      isValid = false;
      alert("Please input second value");
    }
    else if(!fields["val1"].match(/^[0-9]*$/)){
      isValid = false;
      alert("Please input valid number");
    }
    else if(!fields["val2"].match(/^[0-9]*$/)){
      isValid = false;
      alert("Please input valid number");
    }
    return isValid;
  }
  handleSubmit(event) {
    if(this.handleValidation()){
      let result = 0;  
      switch (this.state.operator){  
        case '+ Plus':  
            result = parseFloat(this.state.val1)+ parseFloat(this.state.val2);  
            break;  
        case '- Subtract':  
            result = parseFloat(this.state.val1)- parseFloat(this.state.val2);  
            break;  
        case '* Multiply':  
            result = parseFloat(this.state.val1)* parseFloat(this.state.val2);  
            break;  
        case '/ Divide':  
            result = parseFloat(this.state.val1)/parseFloat(this.state.val2);  
            break;  
        case '% Module':
              result = parseFloat(this.state.val1)%parseFloat(this.state.val2);
              break;
        default:  
            break;  
      }  
      this.setState({total: result});
      this.setState({numbers: this.state.numbers.concat(result)});
    }
    event.preventDefault();
  }
  render(){
    let item = this.state.numbers.map((numbers, index) => (
      <History data={numbers} key={index} />
    ));
    return (
      <Container>
        <Row>
          <Col md="12" xl="6" sm="12">
            <Card style={{marginTop: 20, width: '30rem'}}>
              <Card.Img variant="top" src={"../cal.png"}/>
              <Card.Body>
                <Form id="myform">

                  <Form.Group controlId="formGroupText">
                    <Form.Control type="text" name="val1" value={this.state.value} onChange={this.handleChange} />
                  </Form.Group>

                  <Form.Group controlId="formGroupText">
                    <Form.Control type="text" name="val2" value={this.state.value} onChange={this.handleChange}/>
                  </Form.Group>

                    <Form.Control as="select" name="operator" value={this.state.value} onChange={this.handleChange}>
                      <option>+ Plus</option>
                      <option>- Subtract</option>
                      <option>* Multiply </option>
                      <option>/ Divide</option>
                      <option>% Module</option>
                  </Form.Control>
                  <Button value="submit" onClick={this.handleSubmit} style={{marginTop: "20px"}} variant="primary">Calculate</Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
          <Col md="12" xl="6" sm="12">
            <h1 style={{textAlign: "center", paddingTop: 20}}>Result History</h1>
            <ListGroup>{item}</ListGroup>
          </Col>
        </Row>
      </Container>
    );
  }
}
