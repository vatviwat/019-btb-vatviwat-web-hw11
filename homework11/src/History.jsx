import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {ListGroup} from 'react-bootstrap';
export default class History extends React.Component{
    render() {
        return (
          <div>
            <ListGroup.Item>{this.props.data}</ListGroup.Item>
          </div>
        );
    }    
}